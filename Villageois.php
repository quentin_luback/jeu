<?php
class Villageois extends Character
{
    protected $_type = 'villageois';
    protected $_strength = 3;
    protected $_life = 25;
    public $picture = './img/villageois.png';

    public function __construct($data) {
        parent::__construct($data);
    }
}