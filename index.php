<?php
/*
2A Initial
Jeu de combat
LUBACK Quentin - MOUSTY Thomas - DEGUELDER Chloé
*/
require('header.php');

function loadClass($className)
{
    require $className . '.php';
}
spl_autoload_register('loadClass');

require('Character.php');
require ('Actions.php');

$actions = new Actions($db);

if(isset($_POST['fightForm']) || isset($_POST['nextStage'])) { // Affichage du combat
    if($_POST['player1'] != $_POST['player2']) { // Vérifier que ce sont 2 combattants différents
        $req = $db->query('SELECT * FROM players WHERE id="' . $_POST['player1'] . '" OR id="' . $_POST['player2'] . '"');
        $fighters = $req->fetchAll();

        // Définir objet Villageois, Soldat ou Magicien
        $fighter1 = $actions->setTypeObject($fighters[0]);
        $fighter2 = $actions->setTypeObject($fighters[1]);

        if($fighter1->getLife() <= 0 && $fighter1->getLife() <= 0) { // Rediriger au menu si un combattant est mort et que l'utilisateur rafraîchit la page
            header('Location: index.php');
        } else {
            $fighters = $actions->fight($fighter1, $fighter2);
            require('viewFight.php');
        }
    } else {
        header('Location: index.php?e=sameOpponents');
    }
} else {
    require('viewMenu.php');
    if(isset($_POST['addForm'])) { // Création d'un personnage
        if($_POST['typePlayer'] === 'villageois') {
            $character = new Villageois(array(
                'name' => $_POST['namePlayer']
            ));
        } else if($_POST['typePlayer'] === 'soldat') {
            $character = new Soldat(array(
                'name' => $_POST['namePlayer']
            ));
        } else if($_POST['typePlayer'] === 'magicien') {
            $character = new Magicien(array(
                'name' => $_POST['namePlayer']
            ));
        }

        $actions->ajout($character);
    }
    else if(isset($_POST['deleteForm'])) { // Suppression d'un personnage
        echo $_POST['deletePlayer'];
        $actions->delete($_POST['deletePlayer']);
    }
}
require('footer.php');