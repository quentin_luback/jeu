<div class="div_fight">
    <div class="div_opponent">
        <img src="<?= $fighters[0]->picture ?>" alt="">
        <span class="opponentName"><?= ucfirst($fighters[0]->getType()) ?> <?= $fighters[0]->getName() ?></span>
        <p><?= $fighters[0]->getLife() + $fighters[1]->getStrength() ?>PV
            <span class="opponentDmg">(-<?= $fighters[1]->getStrength() ?>)</span> -> <?= $fighters[0]->getLife() ?>PV</p>
    </div>

    <div class="div_opponent">
        <img src="<?= $fighters[1]->picture ?>" alt="">
        <span class="opponentName"><?= ucfirst($fighters[1]->getType()) ?> <?= $fighters[1]->getName() ?></span>
        <p><?= $fighters[1]->getLife() + $fighters[0]->getStrength() ?>PV
            <span class="opponentDmg">(-<?= $fighters[0]->getStrength() ?>)</span> -> <?= $fighters[1]->getLife() ?>PV</p>
    </div>
</div>

<?php
if($fighters[0]->getLife() <= 0) {
?>
    <p class="opponentDead"><?= $fighters[0]->getName() ?> est mort !</p>
<?php
}

if ($fighters[1]->getLife() <= 0) {
?>
    <p class="opponentDead"><?= $fighters[1]->getName() ?> est mort !</p>
<?php
}
?>
<form method="post" action="" class="fightMenu">
    <input type="hidden" name="player1" value="<?= $_POST['player1'] ?>">
    <input type="hidden" name="player2" value="<?= $_POST['player2'] ?>">
    <?php
    // Si les deux joueurs sont toujours en vie, on affiche le bouton
    if(($fighters[0]->getLife() > 0) && ($fighters[1]->getLife() > 0)) {
    ?>
        <input type="submit" name="nextStage" value="Tour suivant" class="nextStage">
    <?php
    }
    ?>
    <input type="submit" name="back" value="Retour au menu" class="backMenu">
</form>
