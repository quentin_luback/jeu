<?php
class Character
{
    protected $_id;
    protected $_name;
    protected $_type;
    protected $_strength;
    protected $_life;

    public function __construct($data)
    {
        $this ->hydrate($data);
    }

    private function hydrate(array $data)
    {
        foreach($data as $key => $value) {
            $method = 'set' . ucfirst($key);

            if(method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param mixed $name
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->_type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->_type = $type;
    }

    /**
     * @return mixed
     */
    public function getStrength()
    {
        return $this->_strength;
    }

    /**
     * @param mixed $strength
     */
    public function setStrength($strength)
    {
        $this->_strength = $strength;
    }

    /**
     * @return mixed
     */
    public function getLife()
    {
        return $this->_life;
    }

    /**
     * @param mixed $life
     */
    public function setLife($life)
    {
        $this->_life = $life;
    }
}


