<?php
class Magicien extends Character
{
    protected $_type = 'magicien';
    protected $_strength = 5;
    protected $_life = 50;
    public $picture = './img/magicien.png';

    public function __construct($data) {
        parent::__construct($data);
    }
}