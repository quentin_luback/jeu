<?php
class Soldat extends Character
{
    protected $_type = 'soldat';
    protected $_strength = 15;
    protected $_life = 15;
    public $picture = './img/soldat.png';

    public function __construct($data) {
        parent::__construct($data);
    }
}