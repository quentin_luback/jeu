 <div class="listForms">
     <!-- Ajout -->
    <form method="post" action="index.php">
        <input type="text" id="namePlayer" name="namePlayer" placeholder="Choisissez votre nom ..." class="namePlayer" required>
        <div class="div_choisirPerso">
            <span class="span_choisirPerso">- Choisissez votre personnage</span>
            <div class="listeTypes">
                <div>
                    <label for="villageois" class="radioVillageois">
                        <input type="radio" id="villageois" name="typePlayer" value="villageois" checked>
                        <img src="./img/villageois.png" alt="Fond"/>
                        <span class="span_radioType">Villageois</span>
                    </label>
                </div>
                <div>
                    <label for="soldat" class="radioSoldat">
                        <input type="radio" id="soldat" name="typePlayer" value="soldat">
                        <img src="./img/soldat.png" alt="Fond"/>
                        <span class="span_radioType">Soldat</span>
                    </label>
                </div>
                <div>
                    <label for="magicien" class="radioMagicien">
                        <input type="radio" id="magicien" name="typePlayer" value="magicien" >
                        <img src="./img/magicien.png" alt="Fond"/>
                        <span class="span_radioType">Magicien</span>
                    </label>
                </div>
            </div>
        </div>
            <input type="submit" name="addForm" value="Créez votre personnage" class="addForm">
    </form>
    <div class="fight_delete_form">
        <!-- Combat -->
        <form method="post" action="index.php" class="fightFormulaire">
            <?php
            $players = $actions->getPlayers(); // Afficher la liste des combattants
            for($i=0; $i <= 1; $i++) {
            ?>
            <select name="<?= 'player' . strval($i+1) ?>">
            <?php
                foreach($players as $onePlayer) {
                    if($onePlayer['life'] > 0) {
            ?>
                    <option value="<?= $onePlayer['id'] ?>"><?= $onePlayer['name'] ?> / <?= ucfirst($onePlayer['type']) ?> / <?= $onePlayer['life'] ?> PV</option>
            <?php
                    }
                }
            ?>
            </select>
            <?php
                if($i==0) {
            ?>
                    <span class="span_vs">VS</span>
            <?php
                }
            }
            ?>
            <input type="submit" name="fightForm" value="Combattre" class="fightForm">
        </form>
        <?php
        if(!empty($_GET['e']) && $_GET['e'] == 'sameOpponents') {
        ?>
            <div class="errorOpponents"><p>Vous ne pouvez pas choisir le même combattant !</p></div>
        <?php
        }
        ?>
        <!-- Suppression -->
        <form method="post" action="index.php" class="deleteFormulaire">
            <select name="deletePlayer">
            <?php
                foreach($players as $onePlayer) {
            ?>
                    <option value="<?= $onePlayer['id'] ?>"><?= $onePlayer['name'] ?> / <?= ucfirst($onePlayer['type']) ?> / <?= $onePlayer['life'] ?> PV</option>
            <?php
                }
            ?>
            </select>
            <input type="submit" name="deleteForm" value="Supprimez le personnage" class="deleteForm">
            </select>
        </form>
    </div>
 </div>