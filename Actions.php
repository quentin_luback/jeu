<?php
try
{
// On se connecte à MySQL 127.0.0.1:3307
    $db = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'root', '');
}
catch(\Exception $e)
{
// En cas d'erreur, on affiche un message et on arrête tout
    die('Erreur : '.$e->getMessage());
}

class Actions
{
    protected $_db;

    public function __construct($db)
    {
        $this->setDb($db);
    }

    public function setDb(PDO $db)
    {
        $this->_db = $db;
    }

    public function ajout(Character $character)
    {
        $req = $this->_db->prepare('INSERT INTO players (name, type, strength, life)
        VALUES(:name, :type, :strength, :life)');
        $req->execute(array(
            'name' => $character->getName(),
            'type' => $character->getType(),
            'strength' => $character->getStrength(),
            'life' => $character->getLife()
        ));
        header('Location: index.php');
    }

    public function fight($fighter1, $fighter2)
    {
        // Modification de la vie des deux combattants
        $fighter1->setLife($fighter1->getLife() - $fighter2->getStrength());
        $fighter2->setLife($fighter2->getLife() - $fighter1->getStrength());

        // Mise à jour dans la BDD
        self::update($fighter1);
        self::update($fighter2);

        // Retourne un tableau avec les caractéristiques des 2 combattants
        $fighters = array($fighter1, $fighter2);
        return $fighters;
    }

    public function update($character)
    {
        $req = $this->_db->prepare('UPDATE players SET life = :life WHERE id = :id');
        $req->execute(array(
            'id' => $character->getId(),
            'life' => $character->getLife()
        ));
    }

    public function delete($deletePlayer)
    {
        $req = $this->_db->prepare('DELETE FROM players WHERE id="' . $deletePlayer . '"');
        $req->execute();
        header('Location: index.php');
    }


    public function getPlayers()
     {
        $req = $this->_db->query('SELECT * FROM players');
        return $players = $req->fetchAll();
    }

    // Définir le type d'objet d'un combattant choisi
    public function setTypeObject(array $fighter)
    {
        if($fighter['type'] == 'villageois') {
            $fighter = new Villageois(array(
                'id' => $fighter['id'],
                'name' => $fighter['name'],
                'type' => $fighter['type'],
                'strength' => $fighter['strength'],
                'life' => $fighter['life']
            ));
        } else if($fighter['type'] === 'soldat') {
            $fighter = new Soldat(array(
                'id' => $fighter['id'],
                'name' => $fighter['name'],
                'type' => $fighter['type'],
                'strength' => $fighter['strength'],
                'life' => $fighter['life']
            ));
        } else if($fighter['type'] === 'magicien') {
            $fighter = new Magicien(array(
                'id' => $fighter['id'],
                'name' => $fighter['name'],
                'type' => $fighter['type'],
                'strength' => $fighter['strength'],
                'life' => $fighter['life']
            ));
        }
        return $fighter;
    }
}